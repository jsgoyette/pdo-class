<?php

include('class.db.php');

function myErrorHandler($error) {
    echo $error;
}

$db = new db('mysql:host=127.0.0.1;dbname=DBNAME', 'DBUSER', 'DBPASS', 'FETCH_ASSOC');
$db->setErrorCallbackFunction('myErrorHandler');

$results = $db->run("SELECT * FROM comments");
echo '<pre>' . print_r($results, TRUE) . '</pre>';

// update
/*$bind = array(
    ":commentid" => '5'
);

$update = array(
    "pageid" => 2,
    'url' => '/inc/another.php',
    'reviewed' => 0
);

$db->update("comments", $update, "commentid = :commentid", $bind);
*/

// create
/*$insert = array(
    "name" => 'Ame dnnd that again',
    "comment" => 'Yo that -> dc man',
    "date" => date('Y-m-d H:i:s'),
    "email" => 'email@test.com',
    "reviewed" => 0,
);
$db->insert("comments", $insert);
*/

// $db->delete('comments', "commentid = '5'");

// retrieve
$results = $db->select("comments", "email = 'lskdjflasd@gmail.com'", '', '*', 'ORDER BY date DESC');
echo '<pre>' . print_r($results, true) . '</pre>';

$bind = array(
    ":commentid" => '3'
);

$results = $db->selectOne("comments", 'commentid = :commentid', $bind);
echo '<pre>' . print_r($results, true) . '</pre>';



////////////////////////////////////////////////////////////////////////////////

// run Method Declaration
// public function run($sql, $bind="") { }
/*
// MySQL
$sql = <<<STR
CREATE TABLE mytable (
    ID int(11) NOT NULL AUTO_INCREMENT,
    FName varchar(50) NOT NULL,
    LName varchar(50) NOT NULL,
    Age int(11) NOT NULL,
    Gender enum(\'male\',\'female\') NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;
STR;
$db->run($sql);

// SQLite
$sql = <<<STR
CREATE TABLE mytable (
    ID INTEGER PRIMARY KEY,
    LName TEXT,
    FName TEXT,
    Age INTEGER,
    Gender TEXT
)
STR;
$db->run($sql);


////////////////////////////////////////////////////////////////////////////////

/*
// select Method Declaration
// public function select($table, $where="", $bind="", $fields="*", $tail='') { }

// SELECT #1
$results = $db->select("mytable");

// SELECT #2
$results = $db->select("mytable", "Gender = 'male'");

// SELECT #3 w/Prepared Statement
$search = "J";
$bind = array(
    ":search" => "%$search"
);
$results = $db->select("mytable", "FName LIKE :search", $bind);
*/

////////////////////////////////////////////////////////////////////////////////

// update Method Declaration
// public function update($table, $info, $where, $bind="") { }

/*
// Update #1
$update = array(
    "FName" => "Jane",
    "Gender" => "female"
);
$db->update("mytable", $update, "FName = 'John'");

// Update #2 w/Prepared Statement
$update = array(
    "Age" => 24
);
$fname = "Jane";
$lname = "Doe";
$bind = array(
    ":fname" => $fname,
    ":lname" => $lname
);
$db->update("mytable", $update, "FName = :fname AND LName = :lname", $bind);

*/

////////////////////////////////////////////////////////////////////////////////


/*

// setErrorCallbackFunction Method Declaration
public function setErrorCallbackFunction($errorCallbackFunction, $errorMsgFormat="html") { }

// The error message can then be displayed, emailed, etc within the callback function.
function myErrorHandler($error) {
    echo $error;
}

$db = new db("mysql:host=127.0.0.1;port=8889;dbname=mydb", "dbuser", "dbpasswd");
$db->setErrorCallbackFunction("myErrorHandler");

// Text Version
$db->setErrorCallbackFunction("myErrorHandler", "text");

// Internal/Built-In PHP Function
$db->setErrorCallbackFunction("echo");

$results = $db->select("mynonexistingtable");

*/
