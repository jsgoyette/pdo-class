<?php

class db extends PDO
{
    private $sql;
    private $bind;
    private $fetchStyle;
    private $error;
    private $errorCallbackFunction;
    private $errorMsgFormat;

    public function __construct($dsn, $user='', $passwd='', $fetch_style='')
    {
        $r = new ReflectionObject($this);
        $this->fetchStyle = $r->getConstant($fetch_style) ?
            $r->getConstant($fetch_style) : PDO::FETCH_OBJ;

        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    public function setErrorCallbackFunction($errorCallbackFunction, $errorMsgFormat='html')
    {
        $this->errorCallbackFunction = !in_array($errorCallbackFunction, array('echo', 'print'))
            && function_exists($errorCallbackFunction) ?
            $errorCallbackFunction : 'print_r';
        $this->errorMsgFormat = in_array($errorMsgFormat, array('html', 'text')) ?
            $errorMsgFormat : 'html';
    }

    public function selectOne($table, $where='', $bind='', $fields='*', $tail='')
    {
        $sql = $this->makeSelectStmt($table, $fields, $where, $tail);
        $data = $this->run($sql, $bind, 'fetch');
        return $data;
    }

    public function select($table, $where='', $bind='', $fields='*', $tail='')
    {
        $sql = $this->makeSelectStmt($table, $fields, $where, $tail);
        return $this->run($sql, $bind);
    }

    private function makeSelectStmt($table, $fields, $where, $tail)
    {
        $sql = 'SELECT ' . $fields . ' FROM ' . $table;
        if (!empty($where))
            $sql .= ' WHERE ' . $where;
        return $sql . ' ' . $tail . ';';
    }

    public function insert($table, $info)
    {
        $fields = $this->filter($table, $info);

        $sql = 'INSERT INTO ' . $table . ' (' . implode(', ', $fields);
        $sql .= ') VALUES (:' . implode(', :', $fields) . ');';

        $bind = array();
        foreach ($fields as $field)
            $bind[":$field"] = $info[$field];

        return $this->run($sql, $bind);
    }

    public function update($table, $info, $where, $bind='')
    {
        $fields = $this->filter($table, $info);
        $fieldSize = sizeof($fields);

        $sql = 'UPDATE ' . $table . ' SET ';
        for ($f = 0; $f < $fieldSize; $f++) {
            if ($f > 0)
                $sql .= ', ';
            $sql .= $fields[$f] . ' = :' . $fields[$f];
        }
        $sql .= ' WHERE ' . $where . ';';

        $bind = $this->cleanup($bind);
        foreach ($fields as $field)
            $bind[":$field"] = $info[$field];

        return $this->run($sql, $bind);
    }

    public function delete($table, $where='', $bind='')
    {
        $sql = 'DELETE FROM ' . $table .
        $sql .= (!empty($where) ? ' WHERE ' . $where : '') . ';';
        $this->run($sql, $bind);
    }

    public function run($sql, $bind='', $fetch_type='')
    {
        $this->sql = trim($sql);
        $this->bind = $this->cleanup($bind);
        $this->error = '';

        try {
            $pdostmt = $this->prepare($this->sql);
            if ($pdostmt->execute($this->bind) !== false) {
                if (preg_match('/^(select|describe|pragma) /i', $this->sql)) {
                    if (method_exists($pdostmt, $fetch_type))
                        return $pdostmt->$fetch_type($this->fetchStyle);
                    else
                        return $pdostmt->fetchAll($this->fetchStyle);
                }
                elseif (preg_match('/^(delete|insert|update) /i', $this->sql))
                    return $pdostmt->rowCount();
            }
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            $this->debug();
            return false;
        }
    }

    private function debug()
    {
        if (!empty($this->errorCallbackFunction)) {
            $error = array('Error' => $this->error);
            if (!empty($this->sql))
                $error['SQL Statement'] = $this->sql;
            if (!empty($this->bind))
                $error['Bind Parameters'] = trim(print_r($this->bind, true));

            $backtrace = debug_backtrace();
            if (!empty($backtrace)) {
                foreach ($backtrace as $info) {
                    if ($info['file'] != __FILE__)
                        $error['Backtrace'] = $info['file'] . ' at line ' . $info['line'];
                }
            }

            $msg = '';
            if ($this->errorMsgFormat == 'html') {
                if (!empty($error['Bind Parameters']))
                    $error['Bind Parameters'] = '<pre>' . $error['Bind Parameters'] . '</pre>';
                $msg .= "\n" . '<div class="db-error">' . "\n\t<h3>SQL Error</h3>";
                foreach ($error as $key => $val)
                    $msg .= "\n\t<label>" . $key . ':</label> ' . $val;
                $msg .= "\n\t</div>\n</div>";
            } elseif ($this->errorMsgFormat == 'text') {
                $msg .= "SQL Error\n" . str_repeat('-', 50);
                foreach ($error as $key => $val)
                    $msg .= "\n\n$key:\n$val";
            }

            $func = $this->errorCallbackFunction;
            $func($msg);
        }
    }

    private function filter($table, $info)
    {
        $driver = $this->getAttribute(PDO::ATTR_DRIVER_NAME);
        if ($driver == 'sqlite') {
            $sql = 'PRAGMA table_info("' . $table . '");';
            $key = 'name';
        } elseif ($driver == 'mysql') {
            $sql = 'DESCRIBE ' . $table . ';';
            $key = 'Field';
        } else {
            $sql = 'SELECT column_name FROM information_schema.columns ';
            $sql .= 'WHERE table_name = "' . $table . '";';
            $key = 'column_name';
        }

        if (false !== ($list = $this->run($sql))) {
            $fields = array();
            foreach ($list as $record) {
                if ($this->fetchStyle == 'FETCH_OBJ')
                    $fields[] = $record->$key;
                else
                    $fields[] = $record[$key];
            }

            return array_values(array_intersect($fields, array_keys($info)));
        }
        return array();
    }

    private function cleanup($bind)
    {
        if (!is_array($bind)) {
            if (!empty($bind))
                $bind = array($bind);
            else
                $bind = array();
        }
        return $bind;
    }
}
